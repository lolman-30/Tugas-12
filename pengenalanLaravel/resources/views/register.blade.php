<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Tugas 12 Pengenalan Laravel || Sign Up</title>
  </head>
  <body>
    <h2>Buat Account Baru!</h2>
    <form action="/register" method="post">
      @csrf
      <label>First name:</label><br /><br />
      <input type="text" name="firstname" /><br /><br />
      <label>Last name:</label><br /><br />
      <input type="text" name="lastname" /><br /><br />
      <label>Gender:</label><br /><br />
      <input type="radio" name="gender" />Male<br />
      <input type="radio" name="gender" />Female<br />
      <input type="radio" name="gender" />Other<br /><br />
      <label>Nationality:</label>
      <select name="nationality">
        <br />
        <option value="indonesian">Indonesian</option>
        <option value="singapore">Singapore</option>
        <option value="malaysia">Malaysia</option>
        <option value="australia">Autralia</option></select
      ><br /><br />
      <label>Language Spoken:</label><br /><br />
      <input type="checkbox" name="language" />Bahasa Indonesia<br />
      <input type="checkbox" name="language" />English<br />
      <input type="checkbox" name="language" />Other<br /><br />
      <label>Bio:</label><br /><br />
      <textarea name="message" cols="30" rows="10"></textarea><br />
      <input type="submit" />
    </form>
  </body>
</html>
