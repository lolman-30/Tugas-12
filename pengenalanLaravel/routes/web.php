<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

Route::group(['middleware' => 'web'], function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/register', [AuthController::class, 'showRegistrationForm']);
    Route::post('/register', [AuthController::class, 'processRegistration'])->name('register.submit');
    Route::get('/welcome', [AuthController::class, 'showWelcome']);
});
