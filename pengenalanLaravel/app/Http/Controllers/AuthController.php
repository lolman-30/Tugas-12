<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showRegistrationForm()
    {
        return view('register');
    }

    public function processRegistration(Request $request)
    {
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');

        return view('welcome', ['firstname' => $firstname, 'lastname' => $lastname]);
    }

    public function showWelcome()
    {
        return view('welcome');
    }
}
